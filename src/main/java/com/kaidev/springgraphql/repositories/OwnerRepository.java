package com.kaidev.springgraphql.repositories;

import com.kaidev.springgraphql.entities.Owner;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface OwnerRepository extends MongoRepository<Owner, String>, OwnerRepositoryCustom {
    public Owner findByFirstName(String firstName);
    public List<Owner> findByLastName(String lastName);
}
