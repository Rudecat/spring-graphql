package com.kaidev.springgraphql.repositories;

import com.kaidev.springgraphql.entities.Owner;
import com.mongodb.WriteResult;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

public class OwnerRepositoryImpl implements OwnerRepositoryCustom {
    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public Long updateOwner(String firstName, String lastName){
        Query query = new Query(Criteria.where("firstName").is(firstName));
        Update update = new Update();
        update.set("lastName", lastName);

        UpdateResult result = mongoTemplate.updateFirst(query, update, Owner.class);

        if(result!=null)
            return result.getMatchedCount();
        else
            return Long.valueOf(0);
    }
}
