package com.kaidev.springgraphql.repositories;

import com.kaidev.springgraphql.entities.Owner;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface OwnerRepositoryCustom {
    Long updateOwner(String firstName, String lastName);
}
