package com.kaidev.springgraphql.repositories;

import com.kaidev.springgraphql.entities.Pet;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PetRepository extends JpaRepository<Pet, Long> {
}
