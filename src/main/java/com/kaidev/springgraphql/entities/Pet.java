package com.kaidev.springgraphql.entities;

import com.kaidev.springgraphql.enums.Animal;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="Pets")
public class Pet {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String name;

    @Enumerated(EnumType.STRING)
    private Animal type;

    private int age;

    public Pet(){}

    public Pet(String name, Animal type, Integer age){
        this.setName(name);
        this.setType(type);
        this.setAge(age);
    }
}