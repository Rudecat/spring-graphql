package com.kaidev.springgraphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.kaidev.springgraphql.entities.Pet;
import com.kaidev.springgraphql.repositories.PetRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class Query implements GraphQLQueryResolver {

    private final PetRepository petRepository;

    /**
     * === Example mutation ===
     * {
     *   pets {
     *     name
     *     age
     *     type
     *     id
     *   }
     * }
     *
     * @return Pet object
     */
    public Iterable<Pet> pets(){
        return petRepository.findAll();
    }
}