package com.kaidev.springgraphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.kaidev.springgraphql.entities.Pet;
import com.kaidev.springgraphql.enums.Animal;
import com.kaidev.springgraphql.repositories.PetRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
@AllArgsConstructor
public class Mutation implements GraphQLMutationResolver {
    private PetRepository petRepository;

    /**
     * === Example mutation ===
     * mutation AddPet($name: String, $type: Animal, $age: Int){
     *    addPet(name: $name, type: $type, age: $age){
     *     id,
     *     name,
     *     type,
     *     age
     *   }
     * }
     * === Query Variables ===
     * {
     *   "name": "happy",
     *   "type": "DOG",
     *   "age": 5
     * }
     *
     * @param name the name of the pet
     * @param type the enum of the animal type
     * @param age the age of the pet
     * @return
     */
    @Transactional
    public Pet addPet(String name, Animal type, Integer age){
        return petRepository.save(new Pet(name, type, age));
    }

    /**
     * === Example mutation ===
     * mutation DeletePet($id: Int){
     *    deletePet(id: $id)
     * }
     * === Query Variables ===
     * { "id": 5 }
     *
     * @param id the id of the pet
     * @return
     */
    @Transactional
    public Integer deletePet(Long id){
        petRepository.deleteById(id);
        return 1;
    }
}
