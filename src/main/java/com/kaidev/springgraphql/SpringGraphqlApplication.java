package com.kaidev.springgraphql;

import com.kaidev.springgraphql.entities.Owner;
import com.kaidev.springgraphql.repositories.OwnerRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringGraphqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringGraphqlApplication.class, args);
	}

	@Bean
	CommandLineRunner init(OwnerRepository ownerRepository) {

		return args -> {

			Owner obj = ownerRepository.findByLastName("Wang").get(0);
			System.out.println(obj);

			Owner obj2 = ownerRepository.findByFirstName("Alex");
			System.out.println(obj2);

			Long n = ownerRepository.updateOwner("Alex","Kai");
			System.out.println("Number of records updated : " + n);

		};

	}
}
