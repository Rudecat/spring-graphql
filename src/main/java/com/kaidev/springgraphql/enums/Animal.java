package com.kaidev.springgraphql.enums;

public enum Animal {
    DOG,
    CAT,
    BADGER,
    MAMMOTH
}